package ru.ievseev.callback;

import org.mockserver.client.MockServerClient;
import org.mockserver.mock.Expectation;
import org.mockserver.mock.action.ExpectationResponseCallback;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Arrays;

import static java.util.Arrays.stream;
import static org.mockserver.mock.HttpState.getPort;

public class ModifyRedisKeyCallback implements ExpectationResponseCallback {

    private static String backup;

    public HttpResponse handle(HttpRequest httpRequest) {
        Expectation[] expectations = new MockServerClient("localhost", getPort())
                .retrieveActiveExpectations(httpRequest.withHeaders(new ArrayList<>()));
        HttpResponse httpResponse = HttpResponse.response().withBody("test" + Arrays.toString(expectations));

        if (expectations.length >= 1) {
            Expectation exp = stream(expectations)
                    .filter(expectation -> expectation.getHttpResponse() != null)
                    .findFirst().get();
            httpResponse = exp.getHttpResponse();

            String url = httpResponse.getFirstHeader("X-MOCK-REDIS-URL");
            String key = httpResponse.getFirstHeader("X-MOCK-REDIS-KEY");
            String value = httpResponse.getFirstHeader("X-MOCK-REDIS-VALUE");
            boolean doBackup = httpResponse.getFirstHeader("X-MOCK-REDIS-BACKUP").equalsIgnoreCase("true");
            boolean doReset = httpResponse.getFirstHeader("X-MOCK-REDIS-RESET").equalsIgnoreCase("true");

            Jedis jedis = new Jedis(url);

            if (doBackup) {
                backup = jedis.get(key);
            }
            if ((key != null && !key.equals("")) && (value != null)) {
                jedis.set(key, value);
            }
            if (doReset) {
                jedis.set(key, backup);
            }
        }

        return httpResponse;
    }

}
